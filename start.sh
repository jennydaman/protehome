# provides common script stuff, like
# - help function
# - verbose mode
# - load environment variables from .env
# - assert that required variables are defined
#
# usage:
#   source $(dirname "$(readlink -f "$0")")/start.sh
#   assert_declared NEED THESE VARIABLES || exit $? 

source_dir=$(dirname "$(readlink -f "$0")")
# load environment variables from a file
source $source_dir/.env


function show_help () {
  cat << HELP
$0 [-v] [-h] 

${help:-"No help, you're on your own."}

options:
  -h    show this help message and exit
  -v    verbose
  -t    pretty terminal output
HELP
}


while getopts ":hvt" opt; do
  case $opt in
  h   ) show_help && exit 0 ;;
  v   ) verbose='-v' ;;
  t   ) pretty_terminal=y ;;
  \?  ) printf "%s: -%s\n%s\n" "Invalid option" $OPTARG "Run $0 -h for help."
    exit 1 ;;
  esac
done


# check that a list of variables have all been set
function assert_declared () {
  for var; do
    if [ ! -v $var ]; then
      echo "$var is a required variable"
      return 1
    fi
  done
}

# print command to run before running in verbose mode
function verb () {
  if [ -v verbose ]; then
    set -x
  fi
  $@ # run the given command
  { set +x; } 2> /dev/null
}
