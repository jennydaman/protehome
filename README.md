# ProteHOME

Common scripts I wrote for general computing and my home network.

# Setup

Create a `.env` file, here's an example

```env
BACKUP_RCLONE_DESTINATION=nextcloud:/backups/my_laptop
BACKUP_BASE_FOLDER=$HOME
BACKUP_THESE_FOLDERS=(Documents Music Pictures Videos)
```

Run `./install.sh -v` to use systemd timers.
