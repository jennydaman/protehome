#!/bin/bash -e
# copy files from systemd/ to ~/.local/share/systemd/user/

source $(dirname "$(readlink -f "$0")")/start.sh

destination=~/.local/share/systemd/user/

if [ ! -d $destination ]; then
  mkdir -p $verbose $verbose $destination
fi

cp $verbose $source_dir/systemd/* $destination

# replace "/" with "\/" in string
escaped=$(echo $source_dir | sed 's/\//\\&/g')

for file in $source_dir/systemd/*.service; do
  file=$(basename $file)
  verb sed -i "s/ExecStart=/&$escaped\//" "$destination$file"
done

verb systemctl --user daemon-reload
verb systemctl --user enable --now backup
